import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetIpResponse implements Serializable{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1624807834617586764L;
	public String serialNo;
	public String msg;
	public int code;
	public List<ProxyHost> data;
	
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public List<ProxyHost> getData() {
		if(data==null){
			data=new ArrayList<ProxyHost>();
		}
		return data;
	}
	public void setData(List<ProxyHost> data) {
		this.data = data;
	}
	

}
