import java.io.Serializable;

public class ProxyHost implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6907121349671089162L;
	private String ip;
	private int port;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}

}
