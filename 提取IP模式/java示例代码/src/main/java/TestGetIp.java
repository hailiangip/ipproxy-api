import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestGetIp {


    /**
     * 相关认证参数，请从官网登陆购买订单后获取
     */
    public static final String API_HOST = "http://api.hailiangip.com:8422/api";
    /**
     * 请替换成为您自己的orderId，从官网后台获取。
     */

    public static String orderId = "O19042512543756821214";
    /**
     * 请替换成您自己的secret
     */
    public static String secret = "1b416732c0779ad00f6f75beaaede612";

    /**
     * 测试类中，用来保存提取到的IP隧道地址的map
     */
    public static Map<String, List<ProxyHost>> proxyHostMap = new HashMap();
    /**
     * 最后一次提取IP时留下来的流水号，在调用unbind请求时可能用到
     */
    private static String serialNo = "";
    /**
     * 一次提取的IP数目
     */
    private static int num=10;
    /**
     * 当前时间戳，校验需要
     */
    private static long time = System.currentTimeMillis()/1000;

    public static void main(String[] args) throws InterruptedException {
        String ip = "127.0.0.2";  //请修改成您自己当前的外网IP地址。
        //添加IP白名单之后，才能进行代理访问，否则您的订单会被其他人盗用
        testAddWhiteIp(ip);
        //获取IP地址，获取后的地址，存储在proxyHostMap对象中
        testGetIp();
        //根据最后一次获取时留下的流水号，提取获取到的所有IP隧道地址，设置代理，并且请求和输出结果
        for (final ProxyHost proxyHost : proxyHostMap.get(serialNo)) {
            String destUrl = "http://pv.sohu.com/cityjson";
            // //TODO 这个时候没有添加白名单，应该会报错
            String resp = httpsGet(proxyHost.getIp(), proxyHost.getPort(), destUrl);
            System.out.println(resp);
        }
        //测试如何删除IP白名单，删除之后，该地址就无法正常访问了.
        // testDelWhiteIp(ip);
        // resp=httpsGet(proxyHost.getIp(), proxyHost.getPort(), destUrl);
        // System.out.println(resp);
        //测试如何解绑刚才提取的IP，用于比较特殊的情况，当您提取了大量的IP，并且没有及时释放之后，会占用过多资源，我们将限制您的再次提取，所以必须主动释放一些，或者等待自动释放。
        // testUnbind();
        // resp=httpsGet(proxyHost.getIp(), proxyHost.getPort(), destUrl);
        // System.out.println(resp);

    }

    public static void testAddWhiteIp(String ip) {
        if(!isIp(ip)){
            System.err.println("请输入正确的IP地址,而不是"+ip);
            System.exit(0);
        }
        String sign = createPwd(orderId, secret, time);
        String url = API_HOST + "/addWhiteIp?orderId=" + orderId + "&time=" + time + "&sign=" + sign + "&ip=" + ip;
        System.out.println(url);
        String resp = httpsGet(null, 0, url);
        System.out.println(resp);
    }

    public static void testDelWhiteIp(String ip) {
        String sign = createPwd(orderId, secret, time);
        String url = API_HOST + "/delWhiteIp?orderId=" + orderId + "&time=" + time + "&sign=" + sign + "&ip=" + ip;
        System.out.println(url);
        String resp = httpsGet(null, 0, url);
        System.out.println(resp);
    }

    public static void testUnbind() {

        String url = API_HOST + "/unbind?serialNo=" + serialNo;
        System.out.println(url);
        String resp = httpsGet(null, 0, url);
        System.out.println(resp);
    }

    public static void testGetIp() {

        String sign = createPwd(orderId, secret, time);
        //相关参数含义，请参考文档 https://gitee.com/hailiangip/ipproxy-api/blob/master/README.md
        String url = API_HOST + "/getIp?type=1&num=" + num + "&orderId=" + orderId + "&time=" + time + "&sign=" + sign
                + "&singleIp=1";// 自动失效时间30秒。
        System.out.println(url);
        HttpGet httpGet = new HttpGet(url);
        // 获取httpClient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        try {
            for (int i = 0; i < 1; i++) {
                response = httpClient.execute(httpGet);
                String res = EntityUtils.toString(response.getEntity());
                System.out.println(res);
                Gson gson = new Gson();
                GetIpResponse getIpResponse = gson.fromJson(res, GetIpResponse.class);
                if (getIpResponse.getCode() == 0) {
                    serialNo = getIpResponse.getSerialNo();
                    proxyHostMap.put(serialNo, getIpResponse.getData());
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public static String httpsGet(String proxyHostName, int proxyPort, String destUrl) {
        HttpGet httpGet = new HttpGet(destUrl);
        if (StringUtils.isNotEmpty(proxyHostName)) {
            HttpHost proxyHost = new HttpHost(proxyHostName, proxyPort);
            httpGet.setConfig(RequestConfig.custom().setSocketTimeout(3000).setConnectionRequestTimeout(3000)
                    .setConnectTimeout(3000).setProxy(proxyHost).build());
        }
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            String status = response.getStatusLine().toString();
            return EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            return e.getMessage();
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                if (httpClient != null) {
                    httpClient.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 生成pwd
     *
     * @param orderId
     * @param secret
     * @param time
     * @return
     */
    public static String createPwd(String orderId, String secret, long time) {
        String str1 = String.format("orderId=%s&secret=%s&time=%s", orderId, secret, time);
        String sign = org.apache.commons.codec.digest.DigestUtils.md5Hex(str1).toLowerCase();
        return sign;
    }

    public static boolean isIp(String ip){
        String regex="\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
        Pattern pattern=Pattern.compile(regex);    //编译正则表达式
        Matcher matcher=pattern.matcher(ip);    //创建给定输入模式的匹配器
        boolean bool=matcher.matches();
        if(bool)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
