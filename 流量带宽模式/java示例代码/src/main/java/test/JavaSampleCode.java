package test;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class JavaSampleCode {


    public static void main(String[] args) throws IOException {

        final String proxyHostName = "9705-test.hailiangip.com";
        final int proxyPort = 14223;
        final String orderId = "O18102516XXXXXXXX1718";
        final String secret = "0ff876ce1XXXXXXXXca89fe3069fcd8b";
        final String destUrl = "http://pv.sohu.com/cityjson";
        for (int i = 0; i < 100; i++)
            System.out.print(i);
        System.out.println();
        for (int i = 0; i < 10; i++)
            System.out.println(getAuthorization(orderId, secret));

//        httpGet(proxyHostName, proxyPort, destUrl, orderId, secret);
    }

    /**
     * https请求
     *
     * @param proxyHostName
     * @param proxyPort
     * @param destUrl
     * @param orderId
     * @param secret
     * @return
     */
    public static void httpsGet(String proxyHostName, int proxyPort, String destUrl, String orderId, String secret) throws IOException {
        HttpHost proxyHost = new HttpHost(proxyHostName, proxyPort);
        HttpGet httpGet = setProxyHost(destUrl, proxyHost, orderId, secret);
        //https的client设置了SSL认证信息
        CloseableHttpClient httpClient = createHttpClient(proxyHost, createPwd(orderId, secret));
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            System.out.println("---------返回内容-----------");
            System.out.println(response.getStatusLine());
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.close();
            }
            if (httpClient != null) {
                httpClient.close();
            }
        }
    }

    /**
     * http请求
     *
     * @param hostName
     * @param proxyPort
     * @param destUrl
     * @param orderId
     * @param secret
     * @return
     */
    public static void httpGet(String hostName, int proxyPort, String destUrl, String orderId, String secret) throws IOException {
        HttpHost proxyHost = new HttpHost(hostName, proxyPort);
        HttpGet httpGet = setProxyHost(destUrl, proxyHost, orderId, secret);
        //设置头信息
        httpGet.setHeader("proxy-authorization", getAuthorization(orderId, secret));
        //获取httpClient
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            System.out.println("---------返回内容-----------");
            System.out.println(response.getStatusLine());
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.close();
            }
            if (httpClient != null) {
                httpClient.close();
            }
        }
    }

    private static HttpGet setProxyHost(String strUrl, HttpHost proxyHost, String orderId, String secret) {
        URL url = null;
        try {
            url = new URL(strUrl);
            HttpGet httpGet = new HttpGet(url.getFile());
            httpGet.setConfig(RequestConfig.custom().setProxy(proxyHost).build());
            return httpGet;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static CloseableHttpClient createHttpClient(HttpHost proxyHost, String pwd) {
        try {
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    new SSLContextBuilder().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build());

            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                    new AuthScope(proxyHost.getHostName(), proxyHost.getPort()),
                    new UsernamePasswordCredentials("User", pwd));

            CloseableHttpClient httpclient = HttpClients.custom()
                    .setDefaultCredentialsProvider(credsProvider)
                    .setSSLSocketFactory(sslsf)
                    .build();
            return httpclient;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 生成pwd
     *
     * @param orderId
     * @param secret
     * @return
     */
    public static String createPwd(String orderId, String secret) {
        long time = new Date().getTime() / 1000;
        String str1 = String.format("orderId=%s&secret=%s&time=%s", orderId, secret, time);
        String sign = org.apache.commons.codec.digest.DigestUtils.md5Hex(str1).toLowerCase();
        return String.format("orderId=%s&time=%s&sign=%s", orderId, time, sign);
    }

    /**
     * 获取proxy-authorization请求头的值
     *
     * @param orderId
     * @param secret
     * @return
     */
    public static String getAuthorization(String orderId, String secret) {
        long time = System.currentTimeMillis() / 1000;
        String tmpTxt = String.format("orderId=%s&secret=%s&time=%s", orderId, secret, time);
        String sign = DigestUtils.md5Hex(tmpTxt).toLowerCase();
        String headerName = "Basic ";
        String headerValue = String.format("User:orderId=%s&time=%s&sign=%s", orderId, time, sign);
        return headerName + Base64.encodeBase64String(headerValue.getBytes());
    }

}