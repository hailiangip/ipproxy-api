# 新用户只需要替换14行和15行的orderno和secret即可运行
import sys
import time
import hashlib
import requests
import base64
_version = sys.version_info

is_python3 = (_version[0] == 3)

orderId = "O18102516XXXXXXXX1718"
secret = "0ff876ce1XXXXXXXXca89fe3069fcd8b"
host = "9705-test.hailiangip.com"
port = "14223"
user = "proxy"
timestamp = str(int(time.time()))                # 计算时间戳
txt = "orderId=" + orderId + "&" + "secret=" + secret + "&" + "time=" + timestamp

if is_python3:
    txt = txt.encode()

sign = hashlib.md5(txt).hexdigest()                 # 计算sign
password = 'orderId='+orderId+'&time='+timestamp+'&sign='+sign

proxyUrl = "http://" + user + ":" + password + "@" + host + ":" + port
proxy = {"http": proxyUrl,"https": proxyUrl}

r = requests.get("https://www.baidu.com",proxies=proxy)
print("status Code : " + str(r.status_code))
# if r.status_code is 200:
#     print(r.content)